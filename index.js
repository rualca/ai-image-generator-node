const express = require('express')
const dotenv = require('dotenv').config()
const path = require('path')
const routes = require('./routes/openaiRoutes')
const port = process.env.PORT || 5000

const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use(express.static(path.join(__dirname, 'public')))

app.use(routes)

app.listen(port, () => console.log(`Okay partner, server started on port ${port} 🚀`))
