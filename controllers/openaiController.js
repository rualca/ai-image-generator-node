const { Configuration, OpenAIApi } = require("openai")

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY
})
const openai = new OpenAIApi(configuration)
const sizes = {
  small: '256x256',
  medium: '512x512',
  large: '1024x1024',
}

async function generateImage(req, res) {
  try {
    const { prompt, size } = req.body

    const openaiResponse = await openai.createImage({
      prompt,
      n: 1,
      size: sizes[size],
    })

    const { data: [{ url: imageURL }]} = openaiResponse.data

    res.status(200).json({
      success: true,
      data: imageURL
    })
  } catch (error) {
    const { response, message } = error

    if (response) {
      console.log({
        status: response.status,
        data: response.data
      });
    }

    if (message) {
      console.log(error.message);
    }

    res.status(400).json({
      success: false,
      error: `The image can't be generated`
    })
  }
}

module.exports = {
  generateImage
}